{
  const {
    html,
  } = Polymer;
  /**
    `<cells-icon>` Description.

    Example:

    ```html
    <cells-icon></cells-icon>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-icon | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsIcon extends Polymer.Element {

    static get is() {
      return 'cells-icon';
    }

    static get properties() {
      return {
        list: {
          type: Array,
          value() {
            return [];
          }
        }
      };
    }

    clickedIcon(e) {
      this.dispatchEvent(new CustomEvent('clicked', {'detail': {clicked: true}}));
    }

  }

  customElements.define(CellsIcon.is, CellsIcon);
}