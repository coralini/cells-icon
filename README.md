# &lt;cells-icon&gt;

## Description

![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)

```html
<cells-icon></cells-icon>
```

This is a component where you can add a set of icons, specifying the color, the icon and the value for each one of the items on the list.


## Icons

Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.

## Example

```html
<cells-icon
          list='[{"value": "24", "color": "red", "icon": "icons:favorite"}]' 
          ></cells-icon>
```

## Events

clicked: CustomEvent
Fired when an icon is clicked.


## Styling
  The following custom properties and mixins are available for styling:

  ### Custom Properties
  | Custom Property            | Selector | CSS Property | Value       |
  | -------------------------- | -------- | ------------ | ----------- |
  | --cells-fontDefault        | :host    | font-family  |  sans-serif |
  | --cells-icon-display       | span     | display      |  block      |
  | --cells-icon-color-default | default  | color        |  gray       |
  | --cells-icon-color-red     | red      | color        |  red        |
  | --cells-icon-color-blue    | blue     | color        |  blue       |
  | --cells-icon-color-green   | green    | color        |  green      |



  ### @apply
  | Mixins    | Selector | Value |
  | --------- | -------- | ----- |
  | --cells-icon | :host    | {} |
